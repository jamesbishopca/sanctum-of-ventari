(function($) {
  "use strict"; // Start of use strict
  console.log("script loading")
  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    console.log("firing");
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 49)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  $(window).on('scroll', function(event) {
    var scrollValue = $(window).scrollTop();
    if (scrollValue > 70) {
       $('#mainNav').addClass('nav-affixed');
    }
    else if (scrollValue < 70) {
      $('#mainNav').removeClass('nav-affixed');
    }
  });
})(jQuery); // End of use strict
